#!/bin/bash

HOMEDIR=$1

if [[ ! -e "$HOMEDIR/.config" ]]; then
   mkdir $HOMEDIR/.config
fi

for i in alacritty autorandr i3 polybar rofi 
do
   ln -s $PWD/$i/ $HOMEDIR/.config/$i
done

for i in .bashalias .profile .bashrc .bash_profile .vimrc
do 
   ln -s $PWD/$i $HOMEDIR/$i 
done
