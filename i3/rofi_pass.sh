#!/bin/bash

if [[ -z "$@" ]]; then
    find ~/.password-store -type f -printf "%P\n" | sed "s/.\{4\}$//" | grep -ve "^\..*"
else
    pass -c $1 1>/dev/null 2>/dev/null &
fi
