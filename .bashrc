#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

# SSH agent with keychain
eval $(keychain --eval --quiet id_ed25519 id_ed25519_cysec)

# Sources aliases
source ~/.bashalias
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash

# Functions
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Bash Prompt look
export PS1="\[\033[38;5;27m\][\t\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;93m\]\u@\h]\[$(tput sgr0)\]\[\033[38;5;15m\]:\[$(tput sgr0)\]\[\033[38;5;161m\]{\W}\[$(tput sgr0)\]\[\033[1;33m\]\$(parse_git_branch) \[\033[0>0m\]\[$(tput sgr0)\]\\$ "

export TERMINAL=alacritty

# Tell ls to be colourful
export CLICOLOR=1
export LSCOLORS=Exfxcxdxbxegedabagacad

# Tell grep to highlight matches
export GREP_OPTIONS='--color=auto'

export EDITOR=/usr/bin/vim

unset GREP_OPTIONS

#Personal Aliases

alias xclip="xclip -selection c"

# Overwrite TERM variable when using ssh to avoid needing alacritty terminfo
alias ssh="TERM=xterm-256color ssh"

complete -cf sudo
export PATH=$PATH:/opt/VIA/:/opt/fomu-toolchain-linux_x86_64-v1.5.3/bin/:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/qmk_firmware/bin/:$HOME/workspace/nano/buildroot/output/host/usr/bin

rdep() {
   yay -Sii $1 | grep "Required By" | cut -f 8- -d ' '
}

array[0]="red"
array[1]="green"
array[2]="blue"
array[3]="magenta"
array[4]="cyan"
array[5]="white"

size=${#array[@]}
index=$(($RANDOM % $size))

archey3 -c ${array[$index]}
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh

